The goal is to write a very basic "hexdump" which outputs numbers high base numbers, say base 95 (number of printable ASCII characters), then try to compress the output with xz. Basically, I'm trying to see if I can achieve lossless compression. 

Dependencies (for now):
* xxd
* xz
