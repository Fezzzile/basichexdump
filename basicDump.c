#include <stdio.h>

void ascii(int[]);

int main(int argc, char *argv[]){
	
	int i = 0;
	int aile = 1;
	int array[16];

	FILE *fp= fopen(argv[1], "r");
	
	while ((i = getc(fp)) != EOF){
		//printable ascii chars
		//if (i > 31 && i < 127) {
	//	}
		if (aile >= 1 && aile <= 16) {
			array[aile - 1] = i; 
		}
		//make sure it's two characters like xxd and hexdump
		if (i >= 0x0 && i <= 0xf) {
				printf("0%0x", i);

		} else if (i > 0xf) {
				printf("%x", i);
		}

		if (aile < 16) {
			printf(" ");
			aile++;
		} else {
	//		printf("\n");
			aile = 1;
			ascii(array);
		} 
	}
	printf("\n");
	//ascii("nje");
	return 0;
}

void ascii(int array[]) {
	printf("\t\t");
	for (int a = 0; a < 16; a++) { //have to use sizeof() here
		// printable ascii chars
		if (array[a] > 31 && array[a] < 127) {
		putchar(array[a]);
		} else printf(".");
	 } 
	 printf("\n");
}
